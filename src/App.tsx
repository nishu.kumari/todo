/* eslint-disable react/display-name */
/* eslint-disable react/jsx-no-undef */
import Counter from './components/Counter/Counter';
import Button from './components/Button/Button';
import styles from './App.module.scss';
import React, { Component } from 'react';
import { Navbar } from './components/Navbar/Navbar';

type AppState = {
  mount: boolean;
};

export default class extends Component<AppState> {
  mountCounter: () => void;
  unmountCounter: () => void;
  state: AppState = {
    mount: true
  };
  constructor(props: AppState) {
    super(props);
    this.state = {
      mount: true
    };
    this.mountCounter = () => this.setState({ mount: true });
    this.unmountCounter = () => this.setState({ mount: false });
  }

  render() {
    return (
      <>
        <Navbar name="Counter" />
        <div className={styles.div}>
          <Button onClick={this.mountCounter} disabled={this.state.mount}>
            Mount Counter
          </Button>
          {this.state.mount ? <Counter counter={0} error={false} /> : null}
          <Button onClick={this.unmountCounter} disabled={!this.state.mount}>
            unmount Counter
          </Button>
        </div>
      </>
    );
  }
}
