import React, { Component } from 'react';
import styles from './Navbar.module.scss';

export class Navbar extends Component<{ name: string }> {
  render = () => (
    <div className={styles.header}>
      <h2 className={styles.text}>{this.props.name}</h2>
    </div>
  );
}
