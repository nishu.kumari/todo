import React, { Component } from 'react';
import styles from './Button.module.scss';

interface Props {
  children?: React.ReactNode;
  onClick: () => void;
  disabled?: boolean;
}

export default class Button extends Component<Props> {
  render() {
    return (
      <button {...this.props} className={styles.button}>
        {this.props.children}
      </button>
    );
  }
}
